**Lafayette pediatric urgent care**

From newborns to the 18th birthday of the patient, our Pediatric Urgent Care Lafayette provides the finest of advanced and trained medical care. 
Young adults 18-21 years of age with chronic conditions including, but not limited to, 
Cerebral Palsy and Down Syndrome may be admitted by their pediatric or pediatric subspecialist into the Pediatric Unit.
Please Visit Our Website [Lafayette pediatric urgent care](https://urgentcarelafayette.com/pediatric-urgent-care.php) for more information. 

---

## Our pediatric urgent care in Lafayette services

Pediatric patients are cared for by a medically qualified team of physicians and nurses. 
Their experience in treating a variety of pediatric disorders and diseases is similarly matched by their empathy and understanding. 
Our experts shall include: 
Pediatricians 
Pediatric cardiologist  
Pediatric urologists
Our radiologists and radiologists are specialized in pediatric imaging, providing in-house conscious sedation for MRI and CT scans. 
For all patients, our pediatric patients are admitted into the Emergency Room (ED) for special needs. 
Our Pediatric Urgent Care Lafayette Clinic offers a Pediatric Service Area within the ED.
